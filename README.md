A letter where you tell us about yourself, your journey with programming so far and why you think you’re a good fit for the program.
